# Grafana - projet sécurité cloud INSA

- hostname: `heimdall`

# Utilisation

Cloner le repo dans le dossier `/srv/git`

Donner les droit d'execution au script d'installation:
```bash
sudo chmod +x /srv/git/grafana/bin.installation.sh
```

Puis executez le script d'installation:
```bash
/srv/git/grafana/bin.installation.sh
```

## Grafana conf

- id: `admin`
- password: `admin`

## Certificat generation

```bash
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/grafana.key -out /etc/ssl/certs/grafana.crt -subj "/C=FR/ST=France/L=Bourges/O=team-rocket/OU=IT Department/CN=heimdall.valhalla.fr"
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
```
