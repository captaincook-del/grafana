#!/bin/bash

#####################################
#                                   #
#             affichage             #
#                                   #
#####################################

YELLOW='\033[0;33m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0;31m'

function print_begin(){
	echo -e  "${YELLOW}+-----------------------------------------------+"
	echo   "|                                               |"
	printf "|   debut de l'installation pour %-15s|" "$USER"
	echo -e "\n|                                               |"
	echo  -e "+-----------------------------------------------+${NC}"
}

function print_end(){
	echo -e  "${BLUE}+-----------------------------------------------+"
	echo "|                                               |"
	printf "|    fin de l'installation pour %-16s|" "$USER"
	echo -e "\n|                                               |"
	echo  -e "+-----------------------------------------------+${NC}"
}

#####################################
#                                   #
#         main fonction             #
#                                   #
#####################################

function main(){
    print_begin
    install_grafana
    confgi_grafana
    start_service
    generate_cert
    install_nginx
    print_end
}

#####################################
#                                   #
#         grafan fonction           #
#                                   #
#####################################

function install_grafana(){
    echo -e "${YELLOW}----------- Installation de grafana -----------${NC}"
    sudo apt-get install -y apt-transport-https software-properties-common wget
    sudo mkdir -p /etc/apt/keyrings/
    wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null
    echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
    # Updates the list of available packages
    sudo apt-get update
    # Installs the latest OSS release:
    sudo apt-get install grafana -y
}

function confgi_grafana(){
    echo -e "${YELLOW}----------- Configuration de grafana -----------${NC}"
    sudo cp /srv/git/grafana/conf/etc/grafana/grafana.ini /etc/grafana/

}

function start_service(){
    echo -e "${YELLOW}----------- Configuration de grafana -----------${NC}"
    sudo systemctl daemon-reload
    sudo systemctl enable grafana-server
    sudo systemctl start grafana-server
    sudo systemctl status grafana-server
}

function generate_cert(){
    echo -e "${YELLOW}----------- Configuration des certificats -----------${NC}"
    sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/heimdall.key -out /etc/ssl/certs/heimdall.crt -subj "/C=FR/ST=France/L=Bourges/O=team-rocket/OU=IT Department/CN=heimdall.valhalla.fr"
    sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
}

function install_nginx(){
    echo -e "${YELLOW}----------- Installation de nginx -----------${NC}"
    sudo apt-get install nginx -y
    
    sudo cp /srv/git/wikijs/conf/etc/nginx/site-avaible/* /etc/nginx/site-available
    sudo unlink /etc/nginx/site-enabled/default
    sudo ln -s /etc/nginx/site-available/wikijs /etc/nginx/site-enabled/
    sudo ln -s /etc/nginx/site-available/wikijs-ssl /etc/nginx/site-enabled/

    sudo systemctl enable nginx.service
    sudo systemctl start nginx.service
}

main